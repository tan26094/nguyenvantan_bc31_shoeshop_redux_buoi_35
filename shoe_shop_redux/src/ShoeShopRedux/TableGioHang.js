import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ADJ_ITEM_QUANTITY,
  REMOVE_FROM_CART,
} from "./redux/constant/shoeShopConstant";

class TableGioHang extends Component {
  renderContent = () => {
    return this.props.cart.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => this.props.handleAdjustQuantity(item.id, -1)}
              class="btn btn-secondary"
            >
              -
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              onClick={() => this.props.handleAdjustQuantity(item.id, 1)}
              class="btn btn-warning"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveShoe(item.id);
              }}
              class="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContent()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.gioHang,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleRemoveShoe: (id) => {
      dispatch({
        type: REMOVE_FROM_CART,
        payload: id,
      });
    },
    handleAdjustQuantity: (idShoe, amount) => {
      dispatch({
        type: ADJ_ITEM_QUANTITY,
        payload: { idShoe, amount },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableGioHang);
