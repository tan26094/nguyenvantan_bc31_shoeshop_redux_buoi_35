import { shoeArr } from "../../data_ShoeShop";
import {
  ADD_TO_CART,
  ADJ_ITEM_QUANTITY,
  REMOVE_FROM_CART,
} from "../constant/shoeShopConstant";
let initialState = {
  shoeArr: shoeArr,
  gioHang: [],
};

export let shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.gioHang];
      if (index == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case REMOVE_FROM_CART: {
      let idShoe = payload;
      let index = state.gioHang.findIndex((item) => {
        return item.id == idShoe;
      });
      let cloneGioHang = [...state.gioHang];
      if (index !== -1) {
        cloneGioHang.splice(index, 1);
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case ADJ_ITEM_QUANTITY: {
      let idShoe = payload.idShoe;
      let adjust = payload.amount;
      let index = state.gioHang.findIndex((item) => {
        return item.id == idShoe;
      });
      let cloneGioHang = [...state.gioHang];
      if (cloneGioHang[index].soLuong == 1 && adjust == -1) {
        cloneGioHang.splice(index, 1);
      } else {
        cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + adjust;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
