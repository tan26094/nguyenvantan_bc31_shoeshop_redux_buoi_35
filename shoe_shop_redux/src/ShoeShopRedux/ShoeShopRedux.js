import React, { Component } from "react";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div className=" py-5">
        <TableGioHang
          handleRemoveShoe={this.handleRemoveShoe}
          handleAdjustQuantity={this.handleAdjustQuantity}
          // gioHang={this.state.gioHang}
        ></TableGioHang>
        <ListShoe />
      </div>
    );
  }
}
