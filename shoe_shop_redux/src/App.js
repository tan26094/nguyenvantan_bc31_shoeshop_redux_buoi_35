import logo from "./logo.svg";
import "./App.css";
import ListShoe from "./ShoeShopRedux/ListShoe";
import ShoeShopRedux from "./ShoeShopRedux/ShoeShopRedux";

function App() {
  return (
    <div className="container App">
      <ShoeShopRedux />
    </div>
  );
}

export default App;
